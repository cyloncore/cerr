![cerr logo](data/images/cerr.png) cerr
=======================================

cerr is a C++ library for handling error messages. It is similar to error handling in Rust or safety-critical software like Autosar. It offers a replacement to exceptions, offering more controls and awareness to developers.

Development happens in `dev/1` branch.

cerr source code is license under the [MIT License](LICENSE) and is free to everyone to use for any purpose. 
